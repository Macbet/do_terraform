resource "digitalocean_kubernetes_cluster" "k8s" {
  name    = var.k8s_name
  region  = var.region
  version = var.k8s_version
  node_pool {
    name       = var.worker_name
    size       = var.worker_size
    node_count = var.worker_count
  }
}

locals {
  namespaces = toset([
    "production",
    "staging",
    "infra"
  ])
}
resource "kubernetes_namespace" "ns" {
  for_each = local.namespaces
  metadata {
    annotations = {
      name = each.key
    }
    name = each.key
  }
  depends_on = [
    digitalocean_kubernetes_cluster.k8s
  ]
}

