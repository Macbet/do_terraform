variable "do_token" {
  default = ""
  type    = string
}

variable "k8s_name" {
  default = "pepega"
  type    = string
}

variable "region" {
  default = "ams3"
  type    = string
}

variable "k8s_version" {
  default = "1.20.2-do.0"
  type    = string
}

variable "worker_name" {
  default = "pepega-worker"
  type    = string
}

variable "worker_size" {
  default = "s-2vcpu-2gb"
  type    = string

}

variable "worker_count" {
  default = 3
  type    = number

}


variable "bitnami-repo" {
  default = "https://charts.bitnami.com/bitnami"
  type    = string
}

variable "redis-version" {
  default = "14.1.1"
  type    = string
}
variable "rabbitmq-version" {
  default = "8.15.0"
  type    = string
}

variable "postgresql-version" {
  default = "10.4.5"
  type    = string
}