resource "helm_release" "ingress" {
  name       = "nginx-ingress"
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"
  version    = "3.30.0"
  namespace  = kubernetes_namespace.ns["infra"].metadata[0].name
  depends_on = [
    kubernetes_namespace.ns["infra"]
  ]

  set {
    name  = "controller.defaultBackend.enabled"
    value = "true"
  }
}

resource "helm_release" "certmanager" {
  name       = "certmanager"
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  version    = "v1.3.1"
  namespace  = kubernetes_namespace.ns["infra"].metadata[0].name
  depends_on = [
    kubernetes_namespace.ns["infra"]
  ]

  set {
    name  = "leaderElection.namespace"
    value = kubernetes_namespace.ns["infra"].metadata[0].name
  }
  set {
    name  = "installCRDs"
    value = "true"
  }
}


resource "helm_release" "vault" {
  name       = "vault"
  repository = "https://kubernetes-charts.banzaicloud.com"
  chart      = "vault"
  version    = "1.12.0"
  namespace  = kubernetes_namespace.ns["infra"].metadata[0].name
  depends_on = [
    kubernetes_namespace.ns["infra"]
  ]
  values = [
    file("${path.module}/charts/vault/values.yaml")
  ]
}

resource "kubernetes_manifest" "clusterIssuer" {
  provider = kubernetes-alpha

  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind       = "ClusterIssuer"
    metadata = {
      name = "letsencrypt-prod"
    }
    spec = {
      acme = {
        email  = "user@example.com"
        server = "https://acme-v02.api.letsencrypt.org/directory"
        privateKeySecretRef = {
          name = "le-prod-issuer"
        }
        solvers = [
          {
            http01 = {
              ingress = {
                class = "nginx"
              }
            }
          },
        ]
      }
    }

  }
  depends_on = [
    helm_release.certmanager
  ]
}
