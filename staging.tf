resource "helm_release" "redis-staging" {
  name       = "redis"
  repository = var.bitnami-repo
  chart      = "redis"
  version    = var.redis-version
  namespace  = kubernetes_namespace.ns["staging"].metadata[0].name

  set {
    name  = "architecture"
    value = "standalone"
  }
  set {
    name  = "persistence.size"
    value = "1Gi"
  }

  depends_on = [
    kubernetes_namespace.ns["staging"]
  ]
}

resource "helm_release" "rabbitmq-staging" {
  name       = "rabbitmq"
  repository = var.bitnami-repo
  chart      = "rabbitmq"
  version    = var.rabbitmq-version
  namespace  = kubernetes_namespace.ns["staging"].metadata[0].name

  set {
    name  = "clustering.enabled"
    value = "false"
  }
  set {
    name  = "persistence.size"
    value = "1Gi"
  }
  depends_on = [
    kubernetes_namespace.ns["staging"]
  ]
}

resource "helm_release" "postgresql-staging" {
  name       = "postgresql"
  repository = var.bitnami-repo
  chart      = "postgresql"
  version    = var.postgresql-version
  namespace  = kubernetes_namespace.ns["staging"].metadata[0].name

  set {
    name  = "clustering.enabled"
    value = "false"
  }
  set {
    name  = "persistence.size"
    value = "1Gi"
  }
  depends_on = [
    kubernetes_namespace.ns["staging"]
  ]
}